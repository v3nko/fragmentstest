package com.example.q1p.fragmentstest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author Victor Kosenko
 */
public class FragmentC extends Fragment {

    public static FragmentC newInstance() {
        return new FragmentC();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_c, container, false);
        view.findViewById(R.id.bt_do_action).setOnClickListener(actionClickListener);
        return view;
    }

    private View.OnClickListener actionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getFragmentManager().popBackStack(FragmentB.class.getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    };
}
